#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 09:09:22 2020

@author: s19000947
"""
import csv

L_transcrits = []
#filename = "/amuhome/s19000947/Documents/M2/GECO/data/madrid/proteins_737_168372.csv"

#filename = "/amuhome/s19000947/Documents/M2/GECO/data/colombianensi/proteins_72941_412268.csv"

#filename = "/amuhome/s19000947/Documents/M2/GECO/data/chernikova/proteins_737_168372.csv"

#filename = "/amuhome/s19000947/Documents/M2/GECO/data/dubai/proteins_36736_228953.csv"

filename = "/amuhome/s19000947/Documents/M2/GECO/data/canadensis/proteins_1176_170871.csv"

# chernicova proteins_737_168372.csv
with open(filename,"r") as fichier:
    my_reader = csv.reader(fichier, delimiter = ',')
    for row in my_reader:
        for n in row[9].split(';'):
            if n != "Length":
                L_transcrits.append(int(n))

print(L_transcrits)
print("Nombre de CDS: ", len(L_transcrits))

print("Min length CDS: ", min(L_transcrits))
print("Max length CDS: ", max(L_transcrits))

somme = sum(L_transcrits)
mean = somme/ len(L_transcrits)
print("Somme CDS:", somme)

print("Mean: ", mean)

coding_density = (somme/820972)*100
print("Coding density: ", coding_density)
