# Comparative genomics of Rickettsia prowezekii str. Madrid against four species of Rickettsia

## Calculate genomic features
- Python3 language
- file name "genomicFeatures.py" in 'Scripts' folder
- CDS
- Results: number of CDS, min length CDS, max length CDS, mean length CDS, sum of CDS, coding density

## Calculate RSCU and Cumulative Frequency of codon usage per thousand:

- R language
- file name: "cumulFreq_and_RSCU.R"
- Needed packages: Seqinr, ggplot
- Data: CDS sequences (see Data folder), file: "data/X/CDS_X.fna" with X as the name of organism
- Results: RSCU Heatmap, and Cumulative Frequency of codon usage lineplot.

## Calculate ratio Obs/Exp of dinucleotide frequency

- R language
- file name: "diNtsFrequency.R"
- Needed packages: ggplot
- Data:  diNtsFrequencies.csv  file in 'Data' folder
- Result: bar-chart

## Average Nucleotidic Identity (ANI) and Average Amino-acid Identity (AAI)

- Results: ANI_and_AAI.txt file in output folder

## Visualize conserved regions and syntenie between two genomes

- Software: FastANI (https://github.com/ParBLiSS/FastANI)
- Dependencies: 
    - Autoconf (http://www.gnu.org/software/autoconf/)
    - GNU Scientific Library ( http://www.gnu.org/software/gsl/ )
    - Boost Library (http://www.boost.org)
    - Zlib (included with OS X and most Linuxes, http://www.zlib.net)
    - C++ compiler with C++11 and openmp support
- Data: complete genomes of organisms
